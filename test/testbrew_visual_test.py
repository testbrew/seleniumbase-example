from seleniumbase import BaseCase


class VisualLayoutTest(BaseCase):

    def test_testbrew_layout(self):
        self.open('https://testbrew.com/about')
        print('\nChecking the baseline compared to the Baseline in "visual_baseline" folder.')
        self.check_window(name="testbrew", level=1)
