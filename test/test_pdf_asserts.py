from seleniumbase import BaseCase


class PdfTestClass(BaseCase):

    def test_assert_pdf_text(self):
        pdf_path = "http://www.ecc.de/blob/15188/1f5f0a54ebfaf0e71a49fbf18601dc95/invoice-report-summary--irs--delivery---example-data.pdf"

        # Assert PDF contains the expected text on Page 1
        self.assert_pdf_text(
            pdf_path,
            "48,964,833.15", page=1)

        # Assert PDF contains the expected text on any of the pages
        print("Printing first page from: \n%s", pdf_path)

        pdf_text = self.get_pdf_text(pdf_path, page=1)
        print(pdf_text)
