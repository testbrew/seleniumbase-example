from seleniumbase import BaseCase
from seleniumbase.config import settings


class MyTestClass(BaseCase):

    def test_basic_site(self):
        self.open("https://testbrew.com/")
        self.assert_title("Home | Testbrew")
        self.assert_element('a[date-id="header-testbrew"]')
        self.click('a[href="/projects"]')
        self.assert_text("This awesome!! Website.")
        self.go_back()
        self.click('a[href="/about"]')
        self.assert_text("About Roy", '//*[@id="gatsby-focus-wrapper"]/div/main/div/h3')

    def test_project_page(self):
        self.open("https://testbrew.com/projects")
        self.click('a[href="https://gitlab.com/testbrew/website"]')
        self.switch_to_window(1)
        self.assert_exact_text('Project ID: 14437823', '//*[@id="content-body"]/div[2]/div[1]/div[1]/div[1]/div[2]/div[2]/span[1]', timeout=settings.LARGE_TIMEOUT)