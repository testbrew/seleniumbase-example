'''
Google.com testing example
'''

from seleniumbase import BaseCase
from pages.google_objects import HomePage, ResultsPage


class GoogleTests(BaseCase):

    def test_google_dot_com(self):
        self.open('https://google.com/ncr')
        # import time; time.sleep(0.5) # This is needed on slow network
        self.update_text(HomePage.search_box, 'stackoverflow.com')
        self.assert_element(HomePage.list_box)
        self.assert_element(HomePage.search_button)
        self.assert_element(HomePage.feeling_lucky_button)
        self.click(HomePage.search_button)
        self.assert_text('stackoverflow.com', ResultsPage.search_results)
        self.assert_element(ResultsPage.images_link)

    def test_google_dot_com_navigation(self):
        self.test_google_dot_com()
        self.click(ResultsPage.result_box + ResultsPage.get_result(5))
        self.assert_title('Stack Overflow - Wikipedia')
        self.assertEqual(self.get_current_url(), 'https://en.wikipedia.org/wiki/Stack_Overflow')
