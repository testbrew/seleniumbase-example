## SeleniumBase Example
#### Introduction
A small example of the [SeleniumBase] test framework which is based on Python.       
This project started due to this [tweet] which got me 
interested in the [visual inspection features] for websites and pdf files.    

For a more in depth knowledge and an in depth explains of all the features, 
check [SeleniumBase] github project.    

##### Project layout
This project follow's the [Page Object Model] pattern.

##### Requirements
This project has been modified from the [SeleniumBase] project.   
My setup is running on [gitlab] with using a docker image build from the [Dockerfile] in this project. 
The [Dockerfile] is similar to the [SeleniumBase] project (`requirements.txt` is the same).   


##### Setup Locally
- install Python on your local machine.
- To [install] SeleniumBase, you can follow the guide. I have used the `pip` command on my local machine.
```pip install seleniumbase```
- Docker image using the [Dockerfile]
- You can [download] the Webdriver drivers, this will be downloaded during the test execution if the drivers do not exist.

##### CI Pipeline
```
            Build Stage                          Test Stage                    Visual Test stage  
    
+---> + - Docker image on change - + ---> + -------- pdf --------- + -----> + - Visual Chrome - + --->
      |                            |      |                        |        |                   |
      |                            |      |                        |        |                   |
      + ------ Docker Image ------ +      + --- TestBrew basic --- +        + - Visual Firefox  + 
                                          |                        |
                                          |                        |
                                          + - google page object - +

```
    
##### How to run locally
On Windows
```
> python -m pytest  --browser=firefox --html=report.html --headless
```
On Linux
```
> pytest  --browser=chrome --html=report.html
```

##### How to run on CI (using docker)
```
docker run registry.gitlab.com/testbrew/seleniumbase-example pytest --browser=firefox --html=report.html --headless
```

##### Useful flags
Select Browser from the supported drivers.
```
--browser=$BROWSER
```
Get reports, the reports located  under `report.html`
```
--html=report.html
```
Run headless using the flag
```
--headless
```
> Note: There are more flags you can use, so check out the project!

- - -
##### Known issues
- Google changes the search result page locators, the tests might fail due to that inconsistency.

[Dockerfile]: [./Dockerfile]
[gitlab]: https://gitlab.com/testbrew/seleniumbase-example
[SeleniumBase]: https://github.com/seleniumbase/SeleniumBase/
[Page Object Model]: https://www.guru99.com/page-object-model-pom-page-factory-in-selenium-ultimate-guide.html
[install]: https://github.com/seleniumbase/SeleniumBase/#-install-seleniumbase
[download]: https://github.com/seleniumbase/SeleniumBase/#-download-a-webdriver
[visual inspection features]: https://github.com/seleniumbase/SeleniumBase/blob/master/examples/visual_testing/ReadMe.md
[tweet]: https://twitter.com/SeleniumBase/status/1230649961769029637