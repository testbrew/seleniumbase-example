'''
Google.com page objects
'''


class ResultElement(object):
    result_header = '.g'
    result_link = 'a'


class HomePage(object):
    dialog_box = '[role="dialog"] div'
    search_box = 'input[title="Search"]'
    list_box = '[role="listbox"]'
    search_button = 'input[value="Google Search"]'
    feeling_lucky_button = '''input[value="I'm Feeling Lucky"]'''


class ResultsPage(object):
    google_logo = 'img[alt="Google"]'
    images_link = 'link=Images'
    search_results = 'div#center_col'
    result_box = '#rso'

    @staticmethod
    def get_result(child = 1):
        return ' ' + ResultElement.result_header + ':nth-child(' + str(child) + ')' + ' ' + ResultElement.result_link
